from pyueye import ueye
from datetime import datetime
import numpy as np
import ctypes
import cv2
import ImageFilter

def snap_pictures(folder_path):
    created_pics = []
    
    for cam_id in range(1, 15):
        #open the camera and reserve memory for the image
        hcam = ueye.HIDS(cam_id)
        pccmem = ueye.c_mem_p()
        memID = ueye.c_int()
        hWnd = ctypes.c_voidp()
        nBitsPerPixel = ueye.INT(24)
        bytes_per_pixel = int(nBitsPerPixel / 8)
        rectAOI = ueye.IS_RECT()
        ueye.is_InitCamera(hcam, hWnd)
        ueye.is_SetDisplayMode(hcam, 0)
        sensorinfo = ueye.SENSORINFO()
        ueye.is_GetSensorInfo(hcam, sensorinfo)
        ueye.is_AllocImageMem(hcam, sensorinfo.nMaxWidth, sensorinfo.nMaxHeight,24, pccmem, memID)
        ueye.is_SetImageMem(hcam, pccmem, memID)
        ueye.is_SetDisplayPos(hcam, 100, 100)

        # Take the picture
        # todo: snapshot 3 times and take the average
        nret = ueye.is_FreezeVideo(hcam, ueye.IS_WAIT)
        print(nret)

            
        # Prepare the output file in png format
        FileParams = ueye.IMAGE_FILE_PARAMS()
        date_n_time_raw = str(datetime.now()).split(" ")
        date =  date_n_time_raw[0].replace("-","_")
        time =  date_n_time_raw[1].split('.')[0].replace(":","_")

        outfile = "{}\\\\cam{}\\\\d_{}_t_{}_{}.png".format(folder_path,
                                                                        cam_id,
                                                                  date, time,
                                                                  cam_id)
        print(outfile)
        FileParams.pwchFileName = outfile
        FileParams.nFileType = ueye.IS_IMG_PNG
        FileParams.ppcImageMem = None
        FileParams.pnImageID = None

        # Save the output to the file
        nret = ueye.is_ImageFile(hcam, ueye.IS_IMAGE_FILE_CMD_SAVE, FileParams, ueye.sizeof(FileParams))
        print(nret)

        # Properly close the camera and free reserved memory
        ueye.is_FreeImageMem(hcam, pccmem, memID)
        ueye.is_ExitCamera(hcam)

        created_pics.append(outfile)
    

    # Correct the pictures that were just taken
    for pic in created_pics:
        ImageFilter.correct_pic(pic)
