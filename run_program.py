from os import mkdir
from sys import exit, argv
import snapshotter
#import ImageFilter



print("Hi, I will create the folder structure to save the pictures.")
try:
    folder_path = argv[1]
except IndexError:
    folder_path = raw_input("Please tell me where the main output will be (copy paste the path): ")
    folder_path = folder_path.replace("\\","\\\\")
    
print("Saving output in {} (double \\ are normal)".format(folder_path))


# Create cam output folders
for cam_id in range(1, 15):
    try:
        mkdir("{}\\\\cam{}".format(folder_path, cam_id))
        mkdir("{}\\\\cam{}\\\\color_corrected".format(folder_path, cam_id))
    except WindowsError:
        print("The picture folders already exist, I will continue anyway")
        


# test round
snapshotter.snap_pictures(folder_path)
